﻿namespace EnableGotsapon

open System.Collections.Generic
open BepInEx
open OobAPI.Plugin
open OobAPI.Hooks
open UnityEngine
open UnityEngine.SceneManagement

type EnableGotsaponOob() =
    interface OobPlugin with
        override this.Init() =
            SceneLoadedHook.EVENT.Register(this)

    interface SceneLoadedHook.Callback with
        override this.OnSceneLoaded (tag) =
            if tag = SceneTag.newBadgetown_whitebox then
                this.AddGotsapon()

    member _.AddGotsapon () =
        let scene = SceneManager.GetActiveScene()
        let rootList = List(scene.rootCount)
        scene.GetRootGameObjects(rootList)

        let gotsapon = rootList.Find(fun v -> v.name = "gotsapon_root")
        gotsapon.SetActive(true)
        gotsapon.transform.position <- Vector3(19.2f, -0.56f, -25.5f)

        let comp = gotsapon.GetComponent<Gotsapon>() // TODO: Cool gotsapon stuff
        ()

[<BepInPlugin("org.generalprogramming.ooblets.EnableGotsapon", "EnableGotsapon", "1.0.0.0")>]
[<BepInDependency("org.generalprogramming.ooblets.OobAPI")>]
[<BepInProcess("ooblets.exe")>]
type EnableGotsaponBep() =
    inherit BaseUnityPlugin()

    member this.Awake() =
        PluginLoader.loadPlugin(EnableGotsaponOob())
